export const enum Status {
  ENABLED = 0,
  DISABLED = 1,
}

export const enum DeviceStatus {
  NORMAL = 1,
  DELETED = -1,
  DISCARDED = 0,
}

export const enum MessageStatus {
  SCRAP = 1,
  DISTRIBUTE = 2,
}

// 设备部门
export const enum DeviceDepartment {
  BUSINESS = "经营部门",
  PRODUCE = "生产部门",
  ADMIN = "行政部门",
  OTHER = "其他部门",
}

// 设备类型
export const enum DeviceType {
  PRODUCE = "生产设备",
  OFFICE = "办公设备",
  SERVICE = "服务设备",
  OTHER = "其他设备",
}

export const DeviceDepartmentList = [
  DeviceDepartment.BUSINESS,
  DeviceDepartment.PRODUCE,
  DeviceDepartment.ADMIN,
  DeviceDepartment.OTHER,
];
export const DeviceTypeList = [
  DeviceType.PRODUCE,
  DeviceType.OFFICE,
  DeviceType.SERVICE,
  DeviceType.OTHER,
];
