declare module "device" {
  export interface DeviceData {
    id: string;
    name: string;
    code: string;
    type: string;
    department: string;
    label: string;
    model: string;
    serialNumber: string;
    sn: string;
    status: string;
    storageTime: number;
    registrationTime: number;
  }

  export interface DeviceDataDefaultResponse<T> {
    success: boolean;
    message: string;
    content: T;
  }
}
