// 设备信息类型
export const enum DeviceDataType {
  WARNING = "优先数据",
  ORDINARY = "普通数据",
  DANGEROUS = "危险数据",
  OTHER = "其它数据",
}

export const DeviceDataTypeList = [
  DeviceDataType.WARNING,
  DeviceDataType.ORDINARY,
  DeviceDataType.DANGEROUS,
  DeviceDataType.OTHER,
];

// 设备信息状态
export const enum DeviceDataStatus {
  NORMAL = "普通",
  DEALT = "已解决",
  CRITICAL = "危急",
}

export const DeviceDataStatusList = [
  DeviceDataStatus.NORMAL,
  DeviceDataStatus.DEALT,
  DeviceDataStatus.CRITICAL,
];
