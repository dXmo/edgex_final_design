declare module "model" {
  export interface Edgex {
    edgex_id: number;
    user_id: number;
    username: string;
    edgex_name: string;
    prefix: string;
    description: string;
    status: number;
    created_time_stamp: number;
    created_time: string;
    location: string;
    address: string;
    extra: string;
    is_follow: boolean;
  }

  export interface EdgexAppendForm {
    edgex_name: string;
    prefix: string;
    extra: string;
    description: string;
    location: string;
  }

  export interface EdgexModifyForm {
    edgex_id: number;
    edgex_name: string;
    prefix: string;
    extra: string;
    description: string;
    location: string;
  }

  export interface Location {
    province: string;
    city: string;
  }

  export interface User {
    user_id: number;
    username: string;
  }

  export interface LoginForm {
    username: string;
    password: string;
    captcha: string;
  }

  export interface RegisterForm {
    username: string;
    password: string;
  }

  export interface DefaultResponse<T> {
    data: T;
    message: string;
    prompts: string;
    status: number;
  }

  export interface DefaultCloudDeviceResponse<T> {
    code: string;
    message: string;
    data: T;
    count: number;
  }

  export interface FileItem {
    uid: string;
    name?: string;
    status?: string;
    response?: string;
    url?: string;
  }

  export interface FileInfo {
    file: FileItem;
    fileList: FileItem[];
  }

  /**
   * 云端设备
   */
  export interface CloudDeviceAppendForm {
    status: number;
    /**
     * 设备名
     */
    name: string;
    /**
     * 设备编码
     */
    code: string;
    /**
     * 设备类型
     */
    type: string | undefined;
    /**
     * 部门
     */
    department: string | undefined;
    /**
     * 设备品牌
     */
    brand: string;
    /**
     * 设备唯一码
     */
    sn: string;
    /**
     * 入库时间
     */
    inboundAt: string;
    /**
     * 设备登记时间
     */
    registrationAt: string;
    /**
     * 设备添加人
     */
  }

  export interface CloudDeviceUpdateForm extends CloudDeviceAppendForm {
    id: string;
  }

  export interface CloudDeviceDistributeForm {
    equipmentId: string;
    department: string | undefined;
    reason: string;
    qty: string;
  }

  export interface SearchDeviceForm {
    name?: string;
    status?: number;
    code?: string;
    startTime?: string;
    endTime?: string;
    limit?: number;
    offset?: number;
    pageNum?: number;
    pageSize?: number;
  }

  export interface CloudDevice {
    id: string;
    name: string;
    code: string;
    type: string;
    department: string;
    brand: string;
    sn: string;
    status: number;
    inboundAt: string;
    registrationAt: string;
    createdBy: string;
    updatedBy: string;
    deletedBy: string | null;
    createdAt: number;
    updatedAt: number;
    deletedAt: number | null;
  }

  export interface CloudDeviceMessage {
    message_id: string;
    // 1为分发，2为报废；
    type: number;
  }

  export interface ScrapMessage {
    id: string;
    equipmentId: string;
    type: string;
    reason: string;
    status: number;
    createdBy: null | string;
    deletedBy: null | string;
    createdAt: null | string;
    deletedAt: null | string;
  }

  export interface DistributeMessage {
    equipmentId: string;
    department: string;
    reason: string;
    qty: number;
    id: string;
    status: number;
  }

  export type CloudDeviceMessageSum = DistributeMessage | ScrapMessage;

  export interface EdgexInfo {
    edgex_id: number;
    prefix: string;
    edgex_name: string;
    device_amount: number;
  }

  export interface DisplayDateItem {
    date: string;
    mean: number;
  }

  export interface DisplayAmountItem {
    temp: string;
    num: number;
  }

  export interface TempDevice {
    id: string;
    device_name: string;
    temp: number;
    lng: string;
    lat: string;
    time: string;
    time2: string;
  }

  export interface CloudDeviceScrapForm {
    equipmentId: string;
    reason: string;
    type: string;
  }

  export interface SearchMessageForm {
    endTime?: string;
    equipmentId?: string;
    limit?: number;
    offset?: number;
    pageNum?: number;
    pageSize?: number;
    reason?: string;
    startTime?: string;
    status?: number;
  }
}
