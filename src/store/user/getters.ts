// store/user/getters.ts

import { GetterTree } from "vuex";
import { UserState } from "./types";
import { RootState } from "../types";

export const getters: GetterTree<UserState, RootState> = {
  username(state): string | null {
    return state.username;
  },
};
