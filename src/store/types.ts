import { mutationsType as user_mutationsType, UserState } from "./user/types";
import {
  mutationsType as position_mutationsType,
  PositionState,
} from "./position/types";

// 全部的state,导出 Vuex.Store 时使用
export interface RootState {
  user: UserState;
  position: PositionState;
}

export const userMutationsType = user_mutationsType;
export const positionMutationsType = position_mutationsType;
