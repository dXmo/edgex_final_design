// store/position/types.ts

export interface PositionState {
  province: string;
  city: string;
}

export const mutationsType: Record<string, string> = {
  SAVE_POSITION: "savePosition",
  CLEAR_POSITION: "clearPosition",
};
