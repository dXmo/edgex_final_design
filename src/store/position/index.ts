import { Module } from "vuex";
import { PositionState } from "./types";
import { RootState } from "../types";
import { getters } from "./getters";
import { mutations } from "./mutations";
import { actions } from "./actions";
const state: PositionState = {
  province: "",
  city: "",
};
const namespaced = true;
export const position: Module<PositionState, RootState> = {
  namespaced,
  actions,
  state,
  getters,
  mutations,
};
export default state;
