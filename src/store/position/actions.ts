// store/position/actions.ts
import { PositionState } from "./types";
import { ActionTree } from "vuex";

import { RootState } from "../types";

export const actions: ActionTree<PositionState, RootState> = {};
