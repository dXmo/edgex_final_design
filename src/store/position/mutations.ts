// store/position/mutations.ts

import { MutationTree } from "vuex";
import { PositionState } from "./types";
import { mutationsType } from "./types";

const emptyState: Record<string, string> = {
  username: "",
};

export const mutations: MutationTree<PositionState> = {
  /**
   * 更新用户状态
   * @param state 原来的位置状态
   * @param userState 更新后的位置状态
   */
  [mutationsType.SAVE_POSITION](state, positionState: PositionState) {
    state = Object.assign(state, positionState);
  },

  [mutationsType.CLEAR_POSITION](state) {
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    state = Object.assign(state, emptyState);
  },
};
