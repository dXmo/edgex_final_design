// store/position/getters.ts

import { GetterTree } from "vuex";
import { PositionState } from "./types";
import { RootState } from "../types";

export const getters: GetterTree<PositionState, RootState> = {};
