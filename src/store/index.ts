import { createStore } from "vuex";
import { RootState } from "./types";
import { user } from "./user/index";
import { position } from "./position/index";

export default createStore<RootState>({
  modules: {
    user,
    position,
  },
});
