import {
  deviceDataAddReq,
  deviceDataDeleteReq,
  deviceDataGetReq,
  deviceDataListReq,
  deviceDataUpdateReq,
} from "@/api/dataReq";
import { DeviceData } from "device";

export const listDeviceData = async (): Promise<DeviceData[]> => {
  return await deviceDataListReq().then((res) => res.data.content);
};

export const getDeviceData = async (id: string): Promise<DeviceData> => {
  return await deviceDataGetReq(id).then((res) => res.data.content);
};

export const addDeviceData = async (data: DeviceData): Promise<null> => {
  return await deviceDataAddReq(data).then((res) => res.data.content);
};

export const updateDeviceData = async (data: DeviceData): Promise<null> => {
  return await deviceDataUpdateReq(data).then((res) => res.data.content);
};

export const deleteDeviceData = async (id: string): Promise<null> => {
  return await deviceDataDeleteReq(id).then((res) => res.data.content);
};
