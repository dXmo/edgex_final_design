import {
  deviceCreateReq,
  deviceDeleteReq,
  deviceDistributeReq,
  deviceGetReq,
  deviceMessageReq,
  deviceScrapMessageReq,
  deviceScrapReq,
  deviceSearchReq,
  deviceStatusReq,
  deviceUndistributeReq,
  deviceUnscrapReq,
  deviceUpdateReq,
} from "@/api/cloudReq";
import {
  CloudDeviceAppendForm,
  CloudDeviceDistributeForm,
  CloudDeviceScrapForm,
  CloudDeviceUpdateForm,
  SearchDeviceForm,
  SearchMessageForm,
} from "model";
import message from "ant-design-vue/es/message";

const defaultSearch = {
  pageNum: 1,
  pageSize: 999,
};

export const createDevice = async (form: CloudDeviceAppendForm) => {
  await deviceCreateReq(form)
    .then(() => {
      message.success("创建" + form.name + "设备成功");
    })
    .catch((err) => {
      message.error(err.response.message);
    });
};

export const updateDevice = async (form: CloudDeviceUpdateForm) => {
  await deviceUpdateReq(form)
    .then(() => {
      message.success("修改" + form.name + "设备成功");
    })
    .catch((err) => {
      message.error(err.response.message);
    });
};

export const listDevice = async () => {
  return await deviceSearchReq({
    pageNum: 1,
    pageSize: 999,
  }).then((res) => res.data.data);
};

export const searchDevice = async (searchForm: SearchDeviceForm) => {
  Object.assign(searchForm, defaultSearch);
  return await deviceSearchReq(searchForm).then((res) => res.data.data);
};
export const getDevice = async (id: number) => {
  return await deviceGetReq(id).then((res) => res.data.data);
};

export const deleteDevice = async (id: string) => {
  return await deviceDeleteReq(id)
    .then(() => {
      message.success("删除设备 " + id + " 成功");
    })
    .catch((err) => {
      message.error(err.response.message);
    });
};

export const statusDevice = async (id: string, status: number) => {
  return await deviceStatusReq({
    id,
    status,
  })
    .then(() => {
      message.success("修改设备状态成功");
    })
    .catch((err) => {
      message.error(err.response.message);
    });
};

export const distributeDevice = async (form: CloudDeviceDistributeForm) => {
  return await deviceDistributeReq(form)
    .then(() => {
      message.success("分发设备成功");
    })
    .catch((err) => {
      message.error(err.response.message);
    });
};

export const scrapDevice = async (form: CloudDeviceScrapForm) => {
  return await deviceScrapReq(form)
    .then(() => {
      message.success("报废设备成功");
    })
    .catch((err) => {
      message.error(err.response.message);
    });
};

export const listMessage = async () => {
  return await deviceMessageReq(defaultSearch).then((res) => res.data.data);
};

export const listScrapMessage = async () => {
  return await deviceScrapMessageReq(defaultSearch).then(
    (res) => res.data.data
  );
};

export const searchMessage = async (searchForm: SearchMessageForm) => {
  Object.assign(searchForm, defaultSearch);
  return await deviceMessageReq(searchForm).then((res) => res.data.data);
};

export const searchScrapMessage = async (searchForm: SearchMessageForm) => {
  Object.assign(searchForm, defaultSearch);
  return await deviceScrapMessageReq(searchForm).then((res) => res.data.data);
};

export const undistributeDevice = async (id: string) => {
  return await deviceUndistributeReq(id)
    .then(() => {
      message.success("删除分发信息成功");
    })
    .catch((err) => {
      message.error(err.response.message);
    });
};

export const unscrapDevice = async (equipmentId: string) => {
  return await deviceUnscrapReq(equipmentId)
    .then(() => {
      message.success("删除设备报废信息成功");
    })
    .catch((err) => {
      message.error(err.response.message);
    });
};
