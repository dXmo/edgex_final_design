import {
  displayAmountReq,
  displayDataReq,
  displayDateReq,
  displayNowReq,
} from "@/api/displayReq";

export const getDisplayDateChartIn = async () => {
  return await displayDateReq("In").then((res) => res.data);
};

export const getDisplayDateChartOut = async () => {
  return await displayDateReq("Out").then((res) => res.data);
};

export const getDisplayAmountChartIn = async () => {
  return await displayAmountReq("In").then((res) => res.data);
};

export const getDisplayAmountChartOut = async () => {
  return await displayAmountReq("Out").then((res) => res.data);
};

export const getDisplayDataChart = async () => {
  return await displayDataReq().then((res) => res.data);
};

export const getDisplayNow = async () => {
  return await displayNowReq().then((res) => res.data);
};
