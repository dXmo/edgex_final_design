import {
  edgexCreateReq,
  edgexDeleteReq,
  edgexFollowReq,
  edgexGetReq,
  edgexInfoReq,
  edgexListReq,
  edgexNearbyReq,
  edgexSearchReq,
  edgexUnfollowReq,
  edgexUpdateReq,
} from "@/api/edgexReq";
import { goLogin } from "@/utils/routerGuard";
import { EdgexAppendForm, EdgexModifyForm } from "model";
import message from "ant-design-vue/es/message";
import { Random } from "mockjs";
import { Status } from "@/@types/enum";

export const listEdgex = async () => {
  return await edgexListReq
    .then((res) => {
      return res.data.data;
    })
    .catch((err) => {
      if (err.response.status === 401) {
        goLogin();
      }
      return [];
    });
};

export const getEdgex = async (keyword: string) => {
  return await edgexGetReq(keyword)
    .then((res) => {
      return res.data.data[0];
    })
    .catch((err) => {
      if (err.response.status === 401) {
        goLogin();
      }
    });
};

export const searchEdgex = async (
  action: string,
  keyword: string,
  status: number
) => {
  return await edgexSearchReq(action, keyword, status)
    .then((res) => {
      if (status === 1) {
        return res.data.data.filter((item) => item.status === Status.ENABLED);
      } else if (status === 2) {
        return res.data.data.filter((item) => item.status === Status.DISABLED);
      }
      return res.data.data;
    })
    .catch((err) => {
      if (err.response.status === 401) {
        goLogin();
      }
      return [];
    });
};

export const createEdgex = async (form: EdgexAppendForm) => {
  return await edgexCreateReq(form)
    .then(() => {
      message.success("创建Edgex服务成功。");
    })
    .catch((err) => {
      if (err.response.status === 401) {
        goLogin();
      }
      message.error(err.response);
    });
};

export const modifyEdgex = async (form: EdgexModifyForm) => {
  return await edgexUpdateReq(form)
    .then(() => {
      message.success("修改Edgex服务成功。");
    })
    .catch((err) => {
      if (err.response.status === 401) {
        goLogin();
      }
      message.error(err.response);
    });
};

export const deleteEdgex = async (edgex_id: number) => {
  return await edgexDeleteReq(edgex_id)
    .then(() => {
      message.success("删除Edgex服务成功。");
    })
    .catch((err) => {
      if (err.response.status === 401) {
        goLogin();
      }
      message.error(err.response);
    });
};

export const followEdgex = async (edgex_id: number, edgex_name: string) => {
  return await edgexFollowReq(edgex_id, edgex_name)
    .then(() => {
      message.success("关注Edgex服务成功。");
    })
    .catch((err) => {
      if (err.response.status === 401) {
        goLogin();
      }
      message.error(err.response);
    });
};

export const unfollowEdgex = async (edgex_id: number) => {
  return await edgexUnfollowReq(edgex_id)
    .then(() => {
      message.success("取消关注Edgex服务成功。");
    })
    .catch((err) => {
      if (err.response.status === 401) {
        goLogin();
      }
      message.error(err.response);
    });
};

export const nearbyEdgex = async (province: string) => {
  return await edgexNearbyReq({ province }).then((res) => {
    return res.data;
  });
};

export const infoEdgex = async () => {
  return await edgexInfoReq().then((res) => {
    return res.data;
  });
};
