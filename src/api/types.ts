// api/types.js

/**
 * 网站使用的 API 列表
 */
const api: {
  [k: string]: string;
} = {
  LIST_EDGEX: "edgex_admin/edgex/search",
  GET_EDGEX: "edgex_admin/edgex/search",
  CREATE_EDGEX: "edgex_admin/edgex/create",
  UPDATE_EDGEX: "edgex_admin/edgex/update",
  NEARBY_EDGEX: "edgex_admin/edgex/nearby",
  LOGIN: "edgex_admin/user/login",
  LOGOUT: "edgex_admin/user/logout",
  REGISTER: "edgex_admin/user/register",
  SEARCH_EDGEX: "edgex_admin/edgex/search",
  DELETE_EDGEX: "edgex_admin/edgex/delete",
  FOLLOW_EDGEX: "edgex_admin/edgex/follow",
  UNFOLLOW_EDGEX: "edgex_admin/edgex/unfollow",
  INFO_EDGEX: "edgex_admin/edgex/info",
};

for (const key in api) {
  api[key] = "/api/" + api[key];
}

const cloudApi: {
  [k: string]: string;
} = {
  CREATE_DEVICE: "v1/equipment/add",
  SEARCH_DEVICE: "v1/equipment/page",
  INFO_DEVICE: "v1/equipment/info",
  UPDATE_DEVICE: "v1/equipment/update",
  DELETE_DEVICE: "v1/equipment/delete",
  STATUS_DEVICE: "v1/equipment/updateStatus",
  SCRAP_DEVICE: "v1/equipmentScrap/add",
  UNSCRAP_DEVICE: "v1/equipmentScrap/delete",
  DISTRIBUTE_DEVICE: "v1/equipmentDistribute/add",
  UNDISTRIBUTE_DEVICE: "v1/equipmentDistribute/delete",
  MESSAGE_DEVICE: "v1/equipment/message",
  IMPORT_DEVICE: "v1/equipment/import",
  SHOW_DISTRIBUTE_DEVICE: "v1/equipmentDistribute/page",
  SHOW_SCRAP_DEVICE: "v1/equipmentScrap/page",
};

for (const key in cloudApi) {
  cloudApi[key] = "/cloud/" + cloudApi[key];
}

const displayApi: {
  [k: string]: string;
} = {
  DISPLAY_DATE: "device/meanByPlace",
  DISPLAY_AMOUNT: "device/tempNum",
  DISPLAY_DIFF: "device/tempMeanScatter",
  DISPLAY_DATA: "device/allDataNum",
  DISPLAY_NOW: "device/newTemper",
  PARK_SPOT: "spot/EmptySpot",
  PARK_ALL: "spot/getAllSpot",
  PARK_INFO: "spot/getSpotByNum",
  SET_TEMP: "device/setWarningData",
  WARN_DATA: "device/getWarningData",
};

for (const key in displayApi) {
  displayApi[key] = "/dapi/" + displayApi[key];
}

const deviceDataApi: {
  [k: string]: string;
} = {
  DATA_ITEM: "data/search",
  DATA_LIST: "data/list",
  DATA_ADD: "data/add",
  DATA_UPDATE: "data/modify",
  DATA_DELETE: "data/delete",
};

for (const key in deviceDataApi) {
  deviceDataApi[key] = "/cloudapi/" + deviceDataApi[key];
}

Object.assign(api, cloudApi);
Object.assign(api, displayApi);
Object.assign(api, deviceDataApi);

export default api;
