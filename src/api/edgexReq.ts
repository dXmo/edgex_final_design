import request from ".";
import api from "./types";
import { AxiosResponse } from "axios";
import {
  DefaultResponse,
  Edgex,
  EdgexAppendForm,
  EdgexInfo,
  EdgexModifyForm,
} from "model";

export const edgexListReq: Promise<
  AxiosResponse<DefaultResponse<Array<Edgex>>>
> = request.get(api.LIST_EDGEX, {
  params: {
    action: "all",
  },
});

export const edgexGetReq = (
  keyword: string
): Promise<AxiosResponse<DefaultResponse<Array<Edgex>>>> => {
  return request.get(api.GET_EDGEX, {
    params: {
      action: "all",
      keyword,
    },
  });
};

export const edgexCreateReq = (
  body: EdgexAppendForm
): Promise<AxiosResponse<DefaultResponse<null>>> => {
  return request.post(api.CREATE_EDGEX, body);
};

export const edgexUpdateReq = (
  body: EdgexModifyForm
): Promise<AxiosResponse<DefaultResponse<null>>> => {
  return request.post(api.UPDATE_EDGEX, body);
};

export const edgexSearchReq = (
  action: string,
  keyword: string,
  status: number
): Promise<AxiosResponse<DefaultResponse<Array<Edgex>>>> => {
  return request.get(api.SEARCH_EDGEX, {
    params: {
      action,
      keyword,
      status,
      offset: 0,
      count: 12,
    },
  });
};

export const edgexDeleteReq = (
  edgex_id: number
): Promise<AxiosResponse<DefaultResponse<null>>> => {
  return request.post(api.DELETE_EDGEX, {
    edgex_id,
  });
};

export const edgexFollowReq = (
  edgex_id: number,
  edgex_name: string
): Promise<AxiosResponse<DefaultResponse<null>>> => {
  return request.post(api.FOLLOW_EDGEX, {
    edgex_id,
    edgex_name,
  });
};

export const edgexUnfollowReq = (
  edgex_id: number
): Promise<AxiosResponse<DefaultResponse<null>>> => {
  return request.post(api.UNFOLLOW_EDGEX, {
    edgex_id,
  });
};

export const edgexNearbyReq = (body: {
  province: string;
}): Promise<AxiosResponse<Array<Edgex>>> => {
  return request.post(api.NEARBY_EDGEX, body);
};

export const edgexInfoReq = (): Promise<AxiosResponse<Array<EdgexInfo>>> => {
  return request.get(api.INFO_EDGEX);
};
