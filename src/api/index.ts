import axios from "axios";
import mock from "./mock";
import mockCloud from "./mock/cloud";
import mockDisplay from "./mock/display";
import mockData from "./mock/data";

/**
 * 添加这一行就代表使用模拟数据
 */
mock();
// mockCloud();
mockData();
// mockDisplay();

/**
 * 设置网站默认后端调用地址
 */
const request = axios.create({
  withCredentials: true,
});

export default request;
