import Mock, { Random } from "mockjs";
import API from "@/api/types";
import moment from "moment";
import { mockRandomList } from ".";
import { DeviceData, DeviceDataDefaultResponse } from "device";
import { DeviceDataStatusList, DeviceDataTypeList } from "@/@types/device/enum";
import { DeviceDepartmentList } from "@/@types/enum";

export const deviceDataItem = (): DeviceData => {
  const item: DeviceData = {
    id: Random.id(),
    name:
      Random.cword().substring(0, 1) +
      Random.cword().substring(0, 1) +
      "-MockData",
    code: Random.id().substring(10),
    type: DeviceDataTypeList[Random.integer(0, 3)],
    department: DeviceDepartmentList[Random.integer(0, 3)],
    label: Random.word().toUpperCase().substring(0, 3),
    model:
      Random.word().substring(0, 2) +
      Random.word().substring(0, 1) +
      "-" +
      Random.integer(100, 1000),
    serialNumber: Random.guid(),
    sn: Random.guid().substring(0, 6) + Random.domain().substring(0, 3),
    status: DeviceDataStatusList[Random.integer(0, 2)],
    storageTime: moment("2020-" + Random.date("MM-dd")).valueOf(),
    registrationTime: moment("2021-" + Random.date("MM-dd")).valueOf(),
  };
  if (Random.integer(0, 1) === 1) item.type = DeviceDataTypeList[1];
  return item;
};

function deviceDataDefaultResponse<T>(
  content: T
): DeviceDataDefaultResponse<T> {
  const item = {
    success: true,
    message: "成功",
    content: content,
  };
  return item;
}

export const generateDeviceDataList = (): Array<DeviceData> => {
  return mockRandomList(20, 100, deviceDataItem);
};

export default () => {
  Mock.mock(API.DATA_LIST, deviceDataDefaultResponse(generateDeviceDataList()));
  Mock.mock(API.DATA_ADD, deviceDataDefaultResponse(null));
  Mock.mock(API.DATA_UPDATE, deviceDataDefaultResponse(null));
  Mock.mock(/cloudapi\/data\/delete.+/, deviceDataDefaultResponse(null));
  Mock.mock(/cloudapi\/data\/search\/.+/, () =>
    deviceDataDefaultResponse(deviceDataItem())
  );
};
