import Mock, { Random } from "mockjs";
import API from "@/api/types";
import { Edgex, User, Location, EdgexInfo } from "model";
import { Status } from "@/@types/enum";

/**
 * 设置延时
 */
Mock.setup({
  timeout: "200-600", // 表示响应时间介于 200 和 600 毫秒之间，默认值是'10-100'。
});

export const wordList = [
  "inside",
  "dipsen",
  "gunast",
  "myrmeckin",
  "footation",
  "agreefaction",
  "midiquiteture",
  "cinean",
  "lifeose",
  "measureenne",
  "nautosity",
  "hydroant",
  "finory",
  "vildoorarium",
  "darkous",
  "answerid",
  "tomo",
  "authorityacy",
  "terring",
  "exoarian",
  "groupacity",
  "cutate",
  "gustose",
  "rhigarian",
  "lowdom",
  "betteria",
  "the",
  "branchiern",
  "octogenacle",
  "eventical",
  "everyoneing",
  "negdom",
  "silvi",
  "electioness",
  "batling",
  "pagscientistion",
  "thatsive",
  "shortian",
  "propertyage",
  "open",
  "letteruous",
  "decimably",
  "tra",
  "fraternheavyast",
  "theori",
  "artisttic",
  "helio",
  "sentiian",
  "nugose",
  "followeer",
];

export const randomWord = (): string => {
  return wordList[Random.integer(0, 49)];
};

const user = (): User => {
  const item: User = {
    user_id: Random.integer(),
    username: Random.name(),
  };
  return item;
};

const status = (): Status => {
  return Random.integer(0, 1);
};

const location = (): Location => {
  const item: Location = {
    province: Random.province(),
    city: Random.city(),
  };
  return item;
};

const edgex = (): Edgex => {
  const item: Edgex = {
    edgex_id: parseInt(Random.id()),
    user_id: Random.integer(1, 100),
    username: Random.cname(),
    address: Random.ip(),
    edgex_name: Random.cword() + Random.cword() + "-MockService",
    prefix: randomWord(),
    description: Random.cword(),
    status: status(),
    created_time_stamp: Random.integer(1000000000, 1700000000),
    created_time: Random.datetime(),
    location: JSON.stringify(location()),
    extra: randomWord(),
    is_follow: Random.boolean(),
  };
  return item;
};

const edgexInfo = (): EdgexInfo => {
  const item: EdgexInfo = {
    prefix: randomWord(),
    edgex_id: parseInt(Random.id()),
    edgex_name: Random.cword() + Random.cword() + "-MockService",
    device_amount: Random.integer(10, 30),
  };
  return item;
};

export const mockRandomList = (min: number, max: number, generator: Function) =>
  new Array(Random.integer(min, max)).fill(undefined).map(() => generator());

const edgexList = (min = 12, max = 20) => mockRandomList(min, max, edgex);
const edgexInfoList = () => [
  ...mockRandomList(4, 4, edgexInfo),
  Object.assign(edgexInfo(), {
    edgex_name: "其它",
    device_amount: Random.integer(20, 40),
  }),
];

const edgexListSample = edgexList();

/**
 * Mock模拟数据
 */
export default () => {
  Mock.mock(API.LOGIN, user());
  Mock.mock(API.REGISTER, null);
  Mock.mock(API.LOGOUT, null);
  Mock.mock(API.LIST_EDGEX, edgexListSample);
  Mock.mock(/edgex_admin\/edgex\/search.+/, () => {
    return { data: edgexListSample.sort() };
  });
  Mock.mock(API.DELETE_EDGEX, null);
  Mock.mock(API.FOLLOW_EDGEX, null);
  Mock.mock(API.UNFOLLOW_EDGEX, null);
  Mock.mock(API.UPDATE_EDGEX, null);
  Mock.mock(API.CREATE_EDGEX, null);
  Mock.mock(API.NEARBY_EDGEX, edgexList(5, 9));
  Mock.mock(API.INFO_EDGEX, edgexInfoList());
};

// 输出结果
