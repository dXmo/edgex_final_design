import Mock, { Random } from "mockjs";
import {
  CloudDevice,
  CloudDeviceAppendForm,
  CloudDeviceMessage,
  ScrapMessage,
  DistributeMessage,
} from "model";
import API from "@/api/types";
import moment from "moment";
import { mockRandomList } from ".";
import { DeviceDepartmentList, DeviceTypeList } from "@/@types/enum";

Mock.setup({
  timeout: "200-600", // 表示响应时间介于 200 和 600 毫秒之间，默认值是'10-100'。
});

export const ReasonList = [
  "比特币跌了。",
  "比特币涨了。",
  "今天天气太差。",
  "41头牛被雷劈死了。",
  "有轨电车被淘汰。",
  "日本倒核废水。",
  "设备负责人出家了。",
  "马斯克的地下隧道通车了,尽管并它没有达到预期。",
  "特斯拉的电池要采用全新的技术,逐步实现去钴化。",
  "比尔盖茨离婚。",
  "马云退休。",
  "马斯克直言狗狗币是骗局。",
  "3060显卡涨价至8000元。",
  "618显卡还会再涨，比特币依旧无敌。",
  "挖矿致富，别的都寄了。",
];

export const generateDeviceAppendForm = (): CloudDeviceAppendForm => {
  return {
    name:
      Random.cword().substring(0, 1) +
      Random.cword().substring(0, 1) +
      "-MockMachine",
    code: Random.integer(100, 1000).toString(),
    type: DeviceTypeList[Random.integer(0, 3)],
    department: DeviceDepartmentList[Random.integer(0, 3)],
    brand: Random.word().toUpperCase().substring(0, 3),
    sn: Random.id(),
    inboundAt: "2019-" + Random.date("MM-dd"),
    registrationAt: "2020-" + Random.date("MM-dd"),
    status: 0,
  };
};

export const generateDeviceModifyForm = () => {
  return {
    code: Random.integer(100, 1000).toString(),
    type: DeviceTypeList[Random.integer(0, 3)],
    department: DeviceDepartmentList[Random.integer(0, 3)],
    brand: Random.word().toUpperCase().substring(0, 3),
    inboundAt: "2019-" + Random.date("MM-dd"),
    registrationAt: "2020-" + Random.date("MM-dd"),
    createdBy: Random.cname(),
  };
};

export const generateDeviceDistributeForm = () => {
  return {
    department: DeviceDepartmentList[Random.integer(0, 3)],
    reason: ReasonList[Random.integer(0, 9)],
    qty: Random.integer(30, 300).toString(),
  };
};

export const generateDeviceScrapForm = () => {
  return {
    department: DeviceDepartmentList[Random.integer(0, 3)],
    reason: ReasonList[Random.integer(0, 9)],
    type: DeviceTypeList[Random.integer(0, 3)],
  };
};

export const generateDevice = (): CloudDevice => {
  const item: CloudDevice = {
    id: Random.id(),
    name:
      Random.cword().substring(0, 1) +
      Random.cword().substring(0, 1) +
      "-MockMachine",
    code: Random.integer(100, 1000).toString(),
    type: DeviceTypeList[Random.integer(0, 3)],
    department: DeviceDepartmentList[Random.integer(0, 3)],
    brand: Random.word().toUpperCase().substring(0, 3),
    sn: Random.guid(),
    inboundAt: "2018-" + Random.date("MM-dd"),
    registrationAt: "2019-" + Random.date("MM-dd"),
    createdBy: Random.cname(),
    updatedBy: Random.cname(),
    status: Random.integer(-1, 1),
    deletedBy: null,
    createdAt: moment("2018-" + Random.date("MM-dd")).valueOf(),
    updatedAt: moment("2019-" + Random.date("MM-dd")).valueOf(),
    deletedAt: null,
  };
  if (item.status === -1) {
    item.deletedBy = Random.cname();
    item.deletedAt = moment("2020-" + Random.date("MM-dd")).valueOf();
  }
  return item;
};

export const generateDeviceList = (min = 21, max = 30): Array<CloudDevice> => {
  return mockRandomList(min, max, generateDevice);
};

export const generateDistributeMessage = (): DistributeMessage => {
  const item = {
    equipmentId: Random.id(),
    department: DeviceDepartmentList[Random.integer(0, 3)],
    reason: ReasonList[Random.integer(0, 9)],
    qty: Random.integer(10, 100),
    id: Random.id(),
    status: Random.integer(-1, 1),
  };
  return item;
};

export const generateScrapMessage = (): ScrapMessage => {
  const item = {
    reason: ReasonList[Random.integer(0, 9)],
    id: Random.id(),
    equipmentId: Random.id(),
    type: DeviceTypeList[Random.integer(0, 3)],
    status: Random.integer(-1, 1),
    createdBy: Random.name(),
    deletedBy: null,
    createdAt: "2020-" + Random.date("MM-dd"),
    deletedAt: null,
  };
  return item;
};

export const generateMessageList = (): Array<
  ScrapMessage | DistributeMessage
> => {
  const arr = [
    generateScrapMessage(),
    generateDistributeMessage(),
    generateDistributeMessage(),
    generateDistributeMessage(),
    generateScrapMessage(),
    generateDistributeMessage(),
    generateDistributeMessage(),
  ];
  return arr;
};

export default () => {
  // Mock.mock(API.CREATE_DEVICE, null);
  // Mock.mock(API.SEARCH_DEVICE, () => generateDeviceList());
  // Mock.mock(API.UPDATE_DEVICE, null);
  // Mock.mock(API.DELETE_DEVICE, null);
  // Mock.mock(API.STATUS_DEVICE, null);
  // Mock.mock(API.SCRAP_DEVICE, null);
  // Mock.mock(API.UNSCRAP_DEVICE, null);
  // Mock.mock(API.DISTRIBUTE_DEVICE, null);
  // Mock.mock(API.UNDISTRIBUTE_DEVICE, null);
  Mock.mock(/v1\/equipment\/message\/.+/, () => generateMessageList());
};
