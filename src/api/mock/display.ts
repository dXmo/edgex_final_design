import Mock, { Random } from "mockjs";
import API from "@/api/types";
import { TempDevice } from "model";

/**
 * 设置延时
 */
Mock.setup({
  timeout: "200-600", // 表示响应时间介于 200 和 600 毫秒之间，默认值是'10-100'。
});

const dateString = (): string => {
  return "2021-05-14" + "T" + Random.time() + ".000+08:00";
};

const tempDevice = (): TempDevice => {
  const d = dateString();
  const item: TempDevice = {
    id: Random.guid(),
    device_name: Random.name(),
    temp: Random.integer(10, 50),
    lng: Random.integer(100, 130) + "." + Random.integer(100000, 900000),
    lat: Random.integer(30, 70) + "." + Random.integer(100000, 900000),
    time: d,
    time2: d.substring(0, 10) + " " + d.substring(11, 19),
  };
  return item;
};

console.log(tempDevice());

/**
 * Mock模拟数据
 */
export default () => {
  Mock.mock(API.DISPLAY_NOW, tempDevice());
};

// 输出结果
