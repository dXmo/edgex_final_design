import request from ".";
import api from "./types";
import { AxiosResponse } from "axios";
import { DeviceData, DeviceDataDefaultResponse } from "device";

export const deviceDataListReq = (): Promise<
  AxiosResponse<DeviceDataDefaultResponse<Array<DeviceData>>>
> => {
  return request.get(api.DATA_LIST);
};

export const deviceDataGetReq = (
  id: string
): Promise<AxiosResponse<DeviceDataDefaultResponse<DeviceData>>> => {
  return request.get(api.DATA_ITEM + "/" + id);
};

export const deviceDataAddReq = (
  body: DeviceData
): Promise<AxiosResponse<DeviceDataDefaultResponse<null>>> => {
  return request.post(api.DATA_ADD, body);
};

export const deviceDataUpdateReq = (
  body: DeviceData
): Promise<AxiosResponse<DeviceDataDefaultResponse<null>>> => {
  return request.post(api.DATA_UPDATE, body);
};

export const deviceDataDeleteReq = (
  id: string
): Promise<AxiosResponse<DeviceDataDefaultResponse<null>>> => {
  return request.post(api.DATA_DELETE + "?id=" + id, {});
};
