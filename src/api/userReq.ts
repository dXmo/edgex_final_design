import request from ".";
import api from "./types";
import { AxiosResponse } from "axios";
import { LoginForm, RegisterForm } from "model";

export const loginReq = (
  body: LoginForm
): Promise<
  AxiosResponse<{
    data: null;
    message: string;
    prompts: string;
    status: number;
  }>
> => {
  return request.post(api.LOGIN, body);
};

export const registerReq = (
  body: RegisterForm
): Promise<
  AxiosResponse<{
    data: null;
    message: string;
    prompts: string;
    status: number;
  }>
> => {
  return request.post(api.REGISTER, body);
};

export const logoutReq = (): Promise<
  AxiosResponse<{
    data: null | string;
    message: string;
    prompts: string;
    status: number;
  }>
> => {
  return request.get(api.LOGOUT);
};
