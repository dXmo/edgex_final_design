import request from ".";
import api from "./types";
import { AxiosResponse } from "axios";
import { DisplayAmountItem, DisplayDateItem, TempDevice } from "model";

/**
 *
 * @param place In / Out 室内或室外
 * @returns
 */
export const displayDateReq = (
  place: string
): Promise<AxiosResponse<Array<DisplayDateItem>>> => {
  return request.get(api.DISPLAY_DATE + "?place=" + place);
};

export const displayAmountReq = (
  place: string
): Promise<AxiosResponse<Array<DisplayAmountItem>>> => {
  return request.get(api.DISPLAY_AMOUNT + "?place=" + place);
};

export const displayDataReq = (): Promise<AxiosResponse<number>> => {
  return request.get(api.DISPLAY_DATA);
};

export const displayNowReq = (): Promise<AxiosResponse<TempDevice>> => {
  return request.get(api.DISPLAY_NOW);
};
