import request from ".";
import api from "./types";
import { AxiosResponse } from "axios";
import {
  CloudDevice,
  CloudDeviceAppendForm,
  CloudDeviceDistributeForm,
  CloudDeviceUpdateForm,
  DistributeMessage,
  SearchDeviceForm,
  DefaultCloudDeviceResponse,
  CloudDeviceScrapForm,
  SearchMessageForm,
} from "model";

export const deviceCreateReq = (
  body: CloudDeviceAppendForm
): Promise<AxiosResponse<null>> => {
  return request.post(api.CREATE_DEVICE, body);
};

export const deviceUpdateReq = (
  body: CloudDeviceUpdateForm
): Promise<AxiosResponse<null>> => {
  return request.post(api.UPDATE_DEVICE, body);
};

export const deviceSearchReq = (
  body?: SearchDeviceForm
): Promise<AxiosResponse<DefaultCloudDeviceResponse<Array<CloudDevice>>>> => {
  if (body) {
    return request.post(api.SEARCH_DEVICE, body);
  }
  return request.post(api.SEARCH_DEVICE, {});
};

export const deviceGetReq = (
  id: number
): Promise<AxiosResponse<DefaultCloudDeviceResponse<CloudDevice>>> => {
  return request.get(api.INFO_DEVICE + "/" + id);
};

export const deviceDeleteReq = (id: string): Promise<AxiosResponse<null>> => {
  return request.get(api.DELETE_DEVICE + "?id=" + id);
};

export const deviceStatusReq = (body: {
  id: string;
  status: number;
}): Promise<AxiosResponse<null>> => {
  return request.post(api.STATUS_DEVICE, body);
};

export const deviceDistributeReq = (
  body: CloudDeviceDistributeForm
): Promise<AxiosResponse<null>> => {
  return request.post(api.DISTRIBUTE_DEVICE, body);
};

export const deviceScrapReq = (
  body: CloudDeviceScrapForm
): Promise<AxiosResponse<null>> => {
  return request.post(api.SCRAP_DEVICE, body);
};

export const deviceUndistributeReq = (
  id: string
): Promise<AxiosResponse<null>> => {
  return request.get(api.UNDISTRIBUTE_DEVICE + "?id=" + id);
};

export const deviceUnscrapReq = (
  equipmentId: string
): Promise<AxiosResponse<null>> => {
  return request.get(api.UNSCRAP_DEVICE + "?id=" + equipmentId);
};

export const deviceMessageReq = (
  body: SearchMessageForm
): Promise<
  AxiosResponse<DefaultCloudDeviceResponse<Array<DistributeMessage>>>
> => {
  return request.post(api.SHOW_DISTRIBUTE_DEVICE, body);
};

export const deviceScrapMessageReq = (
  body: SearchMessageForm
): Promise<
  AxiosResponse<DefaultCloudDeviceResponse<Array<DistributeMessage>>>
> => {
  return request.post(api.SHOW_SCRAP_DEVICE, body);
};
