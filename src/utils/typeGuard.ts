export function isString(val: unknown): val is string {
  return typeof val === "string";
}

export function isNumber(val: unknown): val is number {
  return typeof val === "number";
}

export function isFunction(val: unknown): val is Function {
  return val instanceof Function;
}

export function isHTMLCanvasElement(val: unknown): val is HTMLCanvasElement {
  return val instanceof HTMLCanvasElement;
}
