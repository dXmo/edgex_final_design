import router from "@/router";
import { docCookies } from "./docCookie";

/**
 * 登出，并且消除本地cookie
 */
export const goLogin = () => {
  docCookies.removeItem("session_token");
  router.push({
    name: "Login",
  });
};
