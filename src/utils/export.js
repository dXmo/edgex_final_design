/* eslint-disable no-undef */

// csv转sheet对象
function csv2sheet(csv) {
  const sheet = {}; // 将要生成的sheet
  csv = csv.split("\n");
  csv.forEach(function (row, i) {
    row = row.split(",");
    if (i == 0)
      sheet["!ref"] =
        "A1:" + String.fromCharCode(65 + row.length - 1) + (csv.length - 1);
    row.forEach(function (col, j) {
      sheet[String.fromCharCode(65 + j) + (i + 1)] = {
        v: col,
      };
    });
  });
  return sheet;
}

// 将一个sheet转成最终的excel文件的blob对象，然后利用URL.createObjectURL下载
function sheet2blob(sheet, sheetName) {
  function s2ab(s) {
    const buf = new ArrayBuffer(s.length);
    const view = new Uint8Array(buf);
    for (let i = 0; i != s.length; ++i) view[i] = s.charCodeAt(i) & 0xff;
    return buf;
  }
  sheetName = sheetName || "sheet1";
  const workbook = {
    SheetNames: [sheetName],
    Sheets: {},
  };
  workbook.Sheets[sheetName] = sheet;
  // 生成excel的配置项
  const wopts = {
    bookType: "xlsx", // 要生成的文件类型
    bookSST: false, // 是否生成Shared String Table，官方解释是，如果开启生成速度会下降，但在低版本IOS设备上有更好的兼容性
    type: "binary",
  };
  const wbout = XLSX.write(workbook, wopts);
  const blob = new Blob([s2ab(wbout)], {
    type: "application/octet-stream",
  });
  // 字符串转ArrayBuffer

  return blob;
}

/**
 * 通用的打开下载对话框方法，没有测试过具体兼容性
 * @param url 下载地址，也可以是一个blob对象，必选
 * @param saveName 保存文件名，可选
 */
function openDownloadDialog(url, saveName) {
  if (typeof url == "object" && url instanceof Blob) {
    url = URL.createObjectURL(url); // 创建blob地址
  }
  const aLink = document.createElement("a");
  aLink.href = url;
  aLink.download = saveName || ""; // HTML5新增的属性，指定保存文件名，可以不要后缀，注意，file:///模式下不会生效
  let event;
  if (window.MouseEvent) event = new MouseEvent("click");
  else {
    event = document.createEvent("MouseEvents");
    event.initMouseEvent(
      "click",
      true,
      false,
      window,
      0,
      0,
      0,
      0,
      0,
      false,
      false,
      false,
      false,
      0,
      null
    );
  }
  aLink.dispatchEvent(event);
}

function saveAs(obj, fileName) {
  const tmpa = document.createElement("a");
  tmpa.download = fileName || "下载";
  tmpa.href = URL.createObjectURL(obj);
  tmpa.click();
  setTimeout(function () {
    URL.revokeObjectURL(obj);
  }, 100);
}

function s2ab(s) {
  if (typeof ArrayBuffer !== "undefined") {
    const buf = new ArrayBuffer(s.length);
    const view = new Uint8Array(buf);
    for (let i = 0; i != s.length; ++i) {
      view[i] = s.charCodeAt(i) & 0xff;
    }
    return buf;
  } else {
    const buf = new Array(s.length);
    for (let i = 0; i != s.length; ++i) {
      buf[i] = s.charCodeAt(i) & 0xff;
    }
    return buf;
  }
}

/**
 *
 * @param {CloudDevice} device
 */
export function exportDeviceExcel(device) {
  let data = new Object();

  data = {
    "!ref": "A1:K100",
    A1: {
      v: "brand",
    },
    B1: {
      v: "code",
    },
    C1: {
      v: "department",
    },
    D1: {
      v: "inboundAt",
    },
    E1: {
      v: "name",
    },
    F1: {
      v: "registrationAt",
    },
    G1: {
      v: "sn",
    },
    H1: {
      v: "type",
    },
    A2: {
      v: device.brand,
    },
    B2: {
      v: device.code,
    },
    C2: {
      v: device.department,
    },
    D2: {
      v: device.inboundAt,
    },
    E2: {
      v: device.name,
    },
    F2: {
      v: device.registrationAt,
    },
    G2: {
      v: device.sn,
    },
    H2: {
      v: device.type,
    },
  };

  const wb = {
    SheetNames: ["Sheet1"],
    Sheets: {
      Sheet1: data,
    },
  };

  const tmpDown = new Blob(
    [
      s2ab(
        // 这里的数据是用来定义导出的格式类型
        XLSX.write(wb, {
          bookType: "xlsx",
          bookSST: false,
          type: "binary",
        })
      ),
    ],
    {
      type: "",
    }
  );
  saveAs(tmpDown, `${device.name}.xlsx`);
}
