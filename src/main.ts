import { createApp } from "vue";
import router from "./router";
import App from "./App.vue";
import store from "./store";
import Antd from "./components/Antd";
import "ant-design-vue/dist/antd.css";
import "./index.css";

const app = createApp(App).use(router).use(store);
Antd(app);

app.mount("#app");
