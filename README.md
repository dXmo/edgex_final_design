# edgex-frontend

本项目基于 `Vite2` 的[脚手架](https://gitee.com/dXmo/xmo-vite2-cli)。

> 脚手架劫持了 `git commit` 事件，帮助你输出规整的 `git commit` 。请使用 `yarn commit` 代替 `git add . && git commit -m *`。

## 依赖或插件

使用 `yarn` 或 `npm` 安装插件，推荐 `yarn` 。

1. vite2
2. typescript
3. vue-router && vuex
4. axios + mockjs
5. eslint + recommend
6. husky@4

## 使用

### 运行与部署

```bash
# 热启动
yarn dev

# 部署
yarn build
```

### git

```bash
# commit
yarn commit

# push
git push
```

## 目录结构

### src

```bash
src
├─@types
├─api
│  └─mock
├─assets
├─components
├─controller
├─layout
├─router
├─store
│  └─user
└─views
```

#### @types

全局类型定义，可以用来定义全局模型。

#### api

`axios` 相关，用于与后台执行交互。

#### assets

存放静态文件或 `css`、`scss` 文件。

#### components

存储全局共享组件。

#### layout

存放布局组件。

#### router

`vue-router` 配置。

#### store

`vuex` 配置。

#### views

存放页面

### 根目录

```bash
xmo-vite2-cli
├─.eslintignore
├─.eslintrc.js
├─.gitignore
├─.huskyrc.json
├─commitlint.config.js
├─index.html
├─package.json
├─README.md
├─.prettierrc.json
├─.prettignore
├─tsconfig.json
├─vite.config.ts
├─yarn.lock
```

#### eslint

`.eslintignore` 忽略
`.eslintrc.js` 配置

#### prettier

`.prettignore` 忽略
`.prettierrc,json` 配置

#### husky

`huskyrc` 配置

#### commitlint

`commitlint.config.js` commitlint配置

## 帮助文档

本项目是类ERP项目，由上侧信息栏，左侧导航栏和中间内容栏组成。

![](https://z3.ax1x.com/2021/06/03/2laaA1.png)]

+ 页面左侧是**导航栏**，点击进行路由跳转。

![2laN7R.png](https://z3.ax1x.com/2021/06/03/2laN7R.png)]

+ 页面右上角是三个信息图标，从左到右依次是帮助文档（本文），人员管理页，登出按钮。

![2laB9K.png](https://z3.ax1x.com/2021/06/03/2laB9K.png)]

![2latB9.png](https://z3.ax1x.com/2021/06/03/2latB9.png)]

+ 管理一级页面右下角是**新增按钮**，云端设备页面还有**批量新增按钮**。

![2laYnJ.png](https://z3.ax1x.com/2021/06/03/2laYnJ.png)]

![2lawh6.png](https://z3.ax1x.com/2021/06/03/2lawh6.png)]

+ 对于可交互访问的卡片，点击可以进入详情页或操作页。

![2ladtx.png](https://z3.ax1x.com/2021/06/03/2ladtx.png)]

+ 操作行为悬浮，将会提示你该行为的操作目的。

![2laD1O.png](https://z3.ax1x.com/2021/06/03/2laD1O.png)]

+ 表单需要符合输入要求，才能允许交互行为。
+ 本网站分为边缘服务，边缘设备操作，云端设备，云端数据和数据分析管理页面，内容丰富，敬请使用。

