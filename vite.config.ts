import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";
import vitePluginImport from "vite-plugin-babel-import";
import externalGlobals from "rollup-plugin-external-globals";
import commonjs from "rollup-plugin-commonjs";
import path from "path";

export default defineConfig({
  plugins: [
    vue(),
    vitePluginImport([
      {
        libraryName: "ant-design-vue",
        libraryDirectory: "es",
        style(name) {
          return `ant-design-vue/lib/${name}/style/index.css`;
        },
      },
    ]),
  ],
  resolve: {
    alias: {
      "@": path.resolve(__dirname, "src"),
      "@c": path.resolve(__dirname, "src/components"),
    },
  },
  server: {
    proxy: {
      "/api": {
        target: "http://106.15.79.230:6789",
        changeOrigin: true,
        rewrite: (path) => path.replace(/^\/api/, ""),
      },
      "/cloud": {
        target: "http://121.41.221.74:8001",
        // target: "http://106.15.79.230:9999",
        changeOrigin: true,
        rewrite: (path) => path.replace(/^\/cloud/, ""),
      },
      "/dapi": {
        target: "http://106.15.79.230:9999",
        changeOrigin: true,
        rewrite: (path) => path.replace(/^\/dapi/, ""),
      },
      "/jwt": {
        target: "http://42.192.203.208:10380",
        changeOrigin: true,
        rewrite: (path) => path.replace(/^\/jwt/, ""),
      },
    },
  },
  build: {
    rollupOptions: {
      // 请确保外部化那些你的库中不需要的依赖
      external: ["mockjs", "axios"],
      output: {
        // 在 UMD 构建模式下为这些外部化的依赖提供一个全局变量
        globals: {
          mockjs: "Mock",
          axios: "axios",
        },
      },
      plugins: [
        commonjs(),
        externalGlobals({ mockjs: "Mock", axios: "axios" }),
      ],
    },
  },
});
